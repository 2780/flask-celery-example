Using Celery with Flask
=======================

This repository contains the example code for my blog article [Using Celery with Flask](http://blog.miguelgrinberg.com/post/using-celery-with-flask).

The application provides two examples of background tasks using Celery:


Quick Setup
-----------

1. Clone this repository.
2. Create a virtualenv and install the requirements.
3. Change path to deploy.sh in app.py
4. Open a second terminal window and start a local Redis server (if you are on Linux or Mac, execute `run-redis.sh` to install and launch a private copy).
5. Open a third terminal window. Start a Celery worker: `venv/bin/celery worker -A app.celery --loglevel=info`.
6. Start the Flask application on your original terminal window: `venv/bin/python app.py`.
7. Start container with name myc. For example `docker run -it --name myc alpine sh`.
8. Start async task with `curl -X POST -i http://localhost:5000/longtask`.
9. Check status at url available in response.

For details on how this all works, see my article [Using Celery with Flask](http://blog.miguelgrinberg.com/post/using-celery-with-flask).
